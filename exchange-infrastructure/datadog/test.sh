#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/datadog-base-image
TAG=observerlive/datadog-base
echo "Testing $PROJECT_DIR"


docker build -t $TAG .
docker run -it --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash $TAG
# docker push ObserverLIVE/olive-it-ci-testos-base
