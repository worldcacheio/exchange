'use strict';

var path = require("path");
var _s = require('underscore.string');

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  var pkg = grunt.file.readJSON("package.json");

  // Configurable paths
  var paths = {
    assets: 'e2e-tests'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    paths: paths,
    env: process.env,
    pkg: pkg,

    // run e2e tests
    protractor: {
      options: {
        configFile: "<%= paths.assets %>/protractor.conf.js",
        // webdriverManagerUpdate: true,
        keepAlive: false,
        noColor: false,
        debug: true
      },
      chrome: {
        options: {
          args: {
            browser:'chrome',
            rootElement: "#observer-live-spa"
          }
        }
      },
      firefox: {
        options: {
          args: {
            browser:'firefox'
          }
        }
      }
    }
  });


  grunt.registerTask('e2e-test', function (browser) {

    grunt.task.run([
      'protractor:' + (browser || 'chrome')
    ]);
  });
};
