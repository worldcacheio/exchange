//jshint strict: false
//var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

//var reporter = new HtmlScreenshotReporter({
//  dest: 'C:/Users/Regimantas/IdeaProjects/t3/screenshots',
// filename: 'my-report.html'
//});


exports.config = {

  // directConnect: false,
  allScriptsTimeout: 11000,
  //getPageTimeout:3000000,
  //rootElement: "body",

    // Run test in sequence using 1 instance

  specs: [

      // Executable tests:
      //   'tests/MainPageTest.js',
      // + 'tests/ServicesPageTest.js',
      // + 'tests/TopAlertsTest.js',
      // 'tests/AddMonitorTest.js',
      // +  'tests/MonitorsPageTest.js',
      // 'tests/TimeSliderTest.js',
      // + 'tests/NocPageTest.js',
      // 'tests/TooltipsTest.js',
      // 'tests/UsersAndGroupsTest.js',
      // 'tests/DetailedServicePageTest.js',
      // 'tests/TenantRegistrationTest.js',
      // 'tests/ConfirmationTest.js'

      // Useful tools:
      // 'customTools/FullMonitorCreationTest.js'
      'customTools/DataFillTests.js'
      // 'customTools/MonitorsCreation.js'
      // 'customTools/BrowsingSimulation.js'
      // 'customTools/FormMonitorList.js'
  ],

  capabilities: {
    browserName: 'chrome',
      // shardTestFiles: true,
    maxInstances: 1,
    chromeOptions: {'args': ['lang=en-GB',  'enable-precise-memory-info' , 'js-flags=--expose-gc', 'no-sandbox']}
  },


    // Run test in parallel using 2 instances

    multiCapabilities: [
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
        // {
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //     sequential: true,
        //     browserName: 'chrome',
        //     specs: ['customTools/BrowsingSimulation.js']
        // },
    ],


  baseUrl: 'http://18.220.229.108/',

  framework: 'jasmine2',

  jasmineNodeOpts: {
    onComplete: null,
    isVerbose: true,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 86400000
  },

  params: {
    toolsData: {
      monitors: 2,
      monitorPrefix: 'OSTest'
    }
  },

  toolsData: {
    monitors: 2,
    monitorPrefix: 'OSTest'
  },

//  beforeLaunch: function() {
  //  return new Promise(function(resolve){
  //   reporter.beforeLaunch(resolve);
  // });
  // },

// Assign the test reporter to each running instance
  // onPrepare: function() {
  //  jasmine.getEnv().addReporter(reporter);
  // },

// Close the report after all tests finish
  // afterLaunch: function(exitCode) {
  ///  return new Promise(function(resolve){
  //     reporter.afterLaunch(resolve.bind(this, exitCode));
  //   });
  // }

};
