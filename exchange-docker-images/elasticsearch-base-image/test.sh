#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/tmp
TAG=exchange/elasticsearch-base-image

docker build -t $TAG .
docker run -it -p 9300:9300/tcp --memory=4g --memory-swap=4g --memory-swappiness=0 $TAG
# docker run -it -e "MAX_MAP_COUNT=262144" --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash $TAG
#docker push $TAG
