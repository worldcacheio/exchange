#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/dd-agent-base-image
TAG=observerlive/dd-agent-base
echo "Testing $PROJECT_DIR"


docker build -t $TAG .
docker run -it --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --log-driver=syslog --memory=4g --memory-swap=4g --memory-swappiness=0 $TAG 
# docker push ObserverLIVE/olive-it-ci-testos-base
