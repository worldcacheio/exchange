#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/tmp
TAG=exchange/java-base-image

docker build -t $TAG .
docker run -it --memory=4g --memory-swap=4g --memory-swappiness=0 $TAG
# docker run -it --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash $TAG
# docker push $TAG
