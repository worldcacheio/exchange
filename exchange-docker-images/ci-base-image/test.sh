#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/exchange-ci-base-image
echo "Testing $PROJECT_DIR"

docker build -t techiebrandon/exchange-ci-base .
docker run -it --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash techiebrandon/exchange-ci-base:latest
# docker push techiebrandon/exchange-ci-base
