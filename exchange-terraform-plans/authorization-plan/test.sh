#!/usr/bin/env bash
PROJECT_DIR=`pwd`
WORKING_DIR=/exchange-authorization-deploy
echo "Testing $PROJECT_DIR"

docker run -it --volume=$PROJECT_DIR:$WORKING_DIR --workdir=$WORKING_DIR --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash techiebrandon/exchange-ci-base:latest
