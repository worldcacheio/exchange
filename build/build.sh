#!/usr/bin/env bash

BASEDIR=$(dirname $0)
PROJECT_ROOT=`pwd`

echo "Project root: ${PROJECT_ROOT}"
export DOCKER_CONFIG=$PROJECT_ROOT/config.json
echo "evo1234" | docker login --username engineering --password -stdin
docker version
docker info
mvn -s $PROJECT_ROOT/settings.xml -Dsettings.security=$PROJECT_ROOT/settings-security.xml deploy -X
#  mvn -s settings.xml -Dsettings.security=settings-security.xml build -X
