version: '3.3'

volumes:
  coinbase_data: {}
  prometheus_data: {}
  grafana_data: {}

networks:
  front_tier:
  back_tier:
  data_tier:
    driver: bridge

services:

  prometheus:
    image: prom/prometheus:v2.0.0
    logging:
      # https://docs.docker.com/engine/admin/logging/gelf/
      driver: "gelf"
      options:
        gelf-address: "udp://localhost:12201"
    volumes:
      - ./exchange-infrastructure/prometheus/:/etc/prometheus/
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/usr/share/prometheus/console_libraries'
      - '--web.console.templates=/usr/share/prometheus/consoles'
    ports:
      - 9090:9090
    links:
      - cadvisor:cadvisor
      - alertmanager:alertmanager
    depends_on:
      - cadvisor
      - graylog
    networks:
      - back_tier
    restart: always
    deploy:
      placement:
        constraints:
          - node.hostname == moby

  node-exporter:
    image: prom/node-exporter
    volumes:
      - /proc:/host/proc:rw
      - /sys:/host/sys:rw
      - /:/rootfs:rw
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 9100:9100
    depends_on:
      - graylog
    networks:
      - back_tier
    restart: always
    deploy:
      mode: global

  alertmanager:
    image: prom/alertmanager
    ports:
      - 9093:9093
    depends_on:
      - graylog
    volumes:
      - ./exchange-infrastructure/alertmanager/:/etc/alertmanager/
    networks:
      - back_tier
    restart: always
    command:
      - '--config.file=/etc/alertmanager/config.yml'
      - '--storage.path=/alertmanager'
    deploy:
      placement:
        constraints:
          - node.hostname == moby
  cadvisor:
    image: google/cadvisor
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
    depends_on:
      - graylog
    ports:
      - 8080:8080
    networks:
      - back_tier
    restart: always
    deploy:
      mode: global

  grafana:
    image: grafana/grafana
    depends_on:
      - prometheus
      - graylog
    ports:
      - 3000:3000
    volumes:
      - grafana_data:/var/lib/grafana
    env_file:
      - ./exchange-infrastructure/config.monitoring
    networks:
      - back_tier
      - front_tier

  # MongoDB: https://hub.docker.com/_/mongo/
  mongo:
    image: mongo:3
  # Elasticsearch: https://www.elastic.co/guide/en/elasticsearch/reference/5.5/docker.html
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:5.6.6
    environment:
      - http.host=0.0.0.0
      # Disable X-Pack security: https://www.elastic.co/guide/en/elasticsearch/reference/5.5/security-settings.html#general-security-settings
      - xpack.security.enabled=false
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    # mem_limit: 1g
  # Graylog: https://hub.docker.com/r/graylog/graylog/
  graylog:
    image: graylog/graylog:2.4.1-1
    environment:
      # CHANGE ME!
      - GRAYLOG_PASSWORD_SECRET=somepasswordpepper
      # Password: admin
      - GRAYLOG_ROOT_PASSWORD_SHA2=8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918
      - GRAYLOG_WEB_ENDPOINT_URI=http://127.0.0.1:9000/api
    links:
      - mongo
      - elasticsearch
    ports:
      # Graylog web interface and REST API
      - 9000:9000
      # Syslog TCP
      - 514:514
      # Syslog UDP
      - 514:514/udp
      # GELF TCP
      - 12201:12201
      # GELF UDP
      - 12201:12201/udp

  # authorization-ms:
  #   image: cb0affa3db1e5b4470087bb272a972ea1bac52eaf6a6c553c4df473945ceee95
  #   networks:
  #     - esnet
  #   ports:
  #     - '80:3001'


  es-datastore:
    image: 79c2751610da
    environment:
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    # mem_limit: 1g
    # cap_add:
    #   - IPC_LOCK
    # environment:
    #   - "constraint:node==node-1"
    #   - bootstrap.memory_lock=true
    #   - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    # ulimits:
    #   memlock:
    #     soft: -1
    #     hard: -1
    # volumes:
    #   - esdata1:/usr/share/elasticsearch/data
    volumes:
      - coinbase_data:/usr/share/elasticsearch/data
    networks:
      - data_tier
    ports:
      - '9300:9300'
      - '9200:9200'
  coinbase-ms:
    image: 02f71fd7a02eecec72e6cb852222eedd9f612879b226d70185c2d92cbe1b3ee3
    depends_on:
      - elasticsearch
    environment:
      - es.transport.host=localhost
    links:
      - elasticsearch
    networks:
      - data_tier
    ports:
      - '81:3001'
