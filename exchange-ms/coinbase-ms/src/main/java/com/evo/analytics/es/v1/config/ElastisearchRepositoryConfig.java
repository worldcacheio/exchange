package com.evo.analytics.es.v1.config;

import java.net.InetSocketAddress;

import javax.annotation.PreDestroy;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = {"com.evo.analytics.es.v1.repository"})

@PropertySource({
	"classpath:elasticsearch.properties"
	})
public class ElastisearchRepositoryConfig {

    @Autowired
    Environment env;

    private Client client;
    private Settings settings;
    
    	@Bean 
    	Settings settings() {
    		settings = Settings.builder()
				.put("cluster.name", env.getProperty("es.cluster.name"))
				.put("client.transport.sniff", env.getProperty("es.client.transport.sniff"))
				.build();
    		return settings;
    	}
        
		@Bean
		public Client client(Settings settings) {
			
			client = TransportClient.builder().settings(settings).build().addTransportAddress(
					new InetSocketTransportAddress(new InetSocketAddress(
	    					env.getProperty("es.transport.host"),
	    					Integer.valueOf(env.getProperty("es.transport.port"))
	    				))
					);
			return client;
		}
		
//		@Bean
//		public ElasticsearchOperations elasticsearchTemplate() {
//		return new ElasticsearchTemplate(nodeBuilder().local(true).node().client());
//		 }
//		}
		
//		@Bean
//		public ElasticsearchTemplate elasticsearchTemplate(Client client) throws Exception {
//			return new ElasticsearchTemplate(client);
//		}
//		
//		@Bean
//		public ElasticsearchOperations createElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
//		    return elasticsearchTemplate;
//		}
		
	    @PreDestroy
	    private void destroy(){
	    	if(null != client){
	    		client.close();
	    	}
	    }
}
