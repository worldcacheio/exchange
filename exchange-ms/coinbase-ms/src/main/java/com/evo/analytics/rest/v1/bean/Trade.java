package com.evo.analytics.rest.v1.bean;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(indexName = "trade-index", type = "trade", shards = 1, replicas = 0, refreshInterval = "-1")
public class Trade implements Serializable {

	private @Id String id;
	private String type;
	private String policy;
	private String status;
	private Double quantity;
	private Double price;
	private Double fee;
	private Date createdAt;
	private Date updatedAt;


	public String generateId() {
    	String plainCreds = createdAt.getTime()+type+policy;
    	byte[] plainCredsBytes = plainCreds.getBytes();
    	byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		return new String(base64CredsBytes);
	}


	public Trade() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Trade(String id, String type, String policy, String status,
			Double quantity, Double price, Double fee, Date createdAt,
			Date updatedAt) {
		super();
		this.id = id;
		this.type = type;
		this.policy = policy;
		this.status = status;
		this.quantity = quantity;
		this.price = price;
		this.fee = fee;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
	
	

//	public Trade(String type, String policy, String status, Double price, Double quantity, Double fee, Date createdAt, Date updatedAt) {
//		super();
//	    this.type = type;
//	    this.policy = policy;
//	    this.status = status;
//	    this.price = price;
//		this.quantity = quantity;
//	    this.fee = fee;
//	    this.createdAt = createdAt;
//		this.id = generateId();
//	}
//	
//	public Trade(String type, String policy, String status, Double price, Double quantity, Double fee) {
//		super();
//	    this.type = type;
//	    this.policy = policy;
//	    this.status = status;
//	    this.price = price;
//		this.quantity = quantity;
//	    this.fee = fee;
//	    this.createdAt = new Date();
//		this.id = generateId();
//	}
	
//	public Trade() {
//		super();
//	}
	
	
}
