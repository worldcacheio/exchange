package com.evo.analytics.rest.v1.service;

import java.text.DecimalFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evo.analytics.es.v1.repository.TradeRepository;
import com.evo.analytics.rest.v1.bean.Trade;

import com.google.common.collect.Lists;
// import com.jdsu.poc.catalyst.rest.v1.enums.MessageType;
import javax.annotation.PostConstruct;

@Service
public class TradeService {


	private static final Logger logger = LoggerFactory.getLogger(TradeService.class);

	@Autowired
	private TradeRepository repository;

	@PostConstruct
	private void setup(){
		clear();
	}


	// public List<Trade> search(Search search){
	// 	List<Alarm> alarms = repository.findByTarget(search.getTarget());
	// 	return alarms;
	// }

	public List<Trade> findAll(){
		return Lists.newArrayList(repository.findAll());
	}

	public List<Trade> create(List<Trade> trades){
		List<Trade> savedTrades = Lists.newArrayList(repository.save(trades));
		return savedTrades;
	}

//	public List<Trade> findByPolicy(String policyName){
//		List<Trade> foundTrades = Lists.newArrayList(repository.findByPolicy(policyName));
//		return foundTrades;
//	}
//
//	public List<Trade> findByStatus(String statusName){
//		List<Trade> foundTrades = Lists.newArrayList(repository.findByStatus(statusName));
//		return foundTrades;
//	}
//
//	public List<Trade> findByType(String typeName){
//		List<Trade> foundTrades = Lists.newArrayList(repository.findByType(typeName));
//		return foundTrades;
//	}

//	public void removeAll(List<Trade> trades){
//		trades.each { trade ->
//			repository.delete(trade.getId());
//		}
//	}

	public void clear(){
		repository.deleteAll();
	}

}
