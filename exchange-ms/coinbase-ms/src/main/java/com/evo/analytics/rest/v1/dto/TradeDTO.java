package com.evo.analytics.rest.v1.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TradeDTO implements Serializable{

  	private String id;
    private String type;
    private String policy;
    private String status;
    private Double quantity;
    private Double price;
    private Double fee;
    private Date createdAt;
    private Date updatedAt;

  	public String getId() {
  		return id;
  	}
  	public void setId(String id) {
  		this.id = id;
  	}

  	public String getPolicy() {
  		return policy;
  	}
  	public void setPolicy(String policy) {
  		this.policy = policy;
  	}

  	public String getStatus() {
  		return status;
  	}
  	public void setStatus(String status) {
  		this.status = status;
  	}

  	public String getType() {
  		return type;
  	}
  	public void setType(String type) {
  		this.type = type;
  	}

  	public Double getPrice() {
  		return price;
  	}
  	public void setPrice(Double price) {
  		this.price = price;
  	}

  	public Double getQuantity() {
  		return quantity;
  	}
  	public void setQuantity(Double quantity) {
  		this.quantity = quantity;
  	}

  	public Double getFee() {
  		return quantity;
  	}
  	public void setFee(Double fee) {
  		this.fee = fee;
  	}

  	public Date getCreatedAt() {
  		return createdAt;
  	}
  	public void setCreatedAt(Date createdAt) {
  		this.createdAt = createdAt;
  	}

  	public Date getUpdatedAt() {
  		return updatedAt;
  	}
  	public void setUpdatedAt(Date updatedAt) {
  		this.updatedAt = updatedAt;
  	}
//    public Trade(String id, String type, String policy, String status,
//      Double price, Double quantity, Double fee,
//      Date createdAt, Date updatedAt) {
////      super();
//      this.id = id;
//      this.type = type;
//      this.policy = policy;
//      this.status = status;
//      this.price = price;
//      this.quantity = quantity;
//      this.fee = fee;
//      this.createdAt = createdAt;
//      this.updatedAt = updatedAt;
//    }
    
    

  	public TradeDTO() {
  		super();
  	}
  	
	public TradeDTO(String id, String type, String policy, String status,
			Double quantity, Double price, Double fee, Date createdAt,
			Date updatedAt) {
		super();
		this.id = id;
		this.type = type;
		this.policy = policy;
		this.status = status;
		this.quantity = quantity;
		this.price = price;
		this.fee = fee;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}


}
