#!/usr/bin/env bash

BASEDIR=$(dirname $0)
PROJECT_ROOT=`pwd`

echo "Project root: ${PROJECT_ROOT}"
export DOCKER_CONFIG=$PROJECT_ROOT/config.json

mvn -s $PROJECT_ROOT/settings.xml clean package deploy
